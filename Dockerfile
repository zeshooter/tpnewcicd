FROM python:3
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY cicd/requirements.txt /code/
RUN pip install -r requirements.txt
COPY cicd/ /code/
RUN apt update && apt install -y default-mysql-client netcat
RUN mkdir /code/staticfiles
RUN chmod +x /code/entrypoint.sh
RUN useradd app
RUN chown -R app:app /code
USER app
ENTRYPOINT ["/code/entrypoint.sh"]
