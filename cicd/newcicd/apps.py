from django.apps import AppConfig


class NewcicdConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'newcicd'
